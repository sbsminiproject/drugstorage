// Fill out your copyright notice in the Description page of Project Settings.


#include "DS_InteractableDoor.h"
#include "DS_Character.h"

void ADS_InteractableDoor::Tick(float _DeltaTime)
{
	Super::Tick(_DeltaTime);

	if (!IsRotating) return;
	FRotator rotation = FMath::RInterpTo(StartRotation, EndRotation, DeltaSeconds += _DeltaTime, DoorSpeed);
	SetActorRotation(rotation);

	if (rotation == EndRotation) {
		DeltaSeconds = 0.f;
		IsRotating = false;

		ActionName = OpenedText;
	}
}

void ADS_InteractableDoor::Interact(ADS_Character* _Character)
{
	FItemState item;

	if (_Character->FindItem(KeyID, item)) {
		IsRotating = true;
		StartRotation = GetActorRotation();
	}
}