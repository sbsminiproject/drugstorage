// Fill out your copyright notice in the Description page of Project Settings.


#include "DS_InteractWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"

void UDS_InteractWidget::Show(FString _ActionName, FString _ObjectName)
{
	ActionText->SetVisibility(ESlateVisibility::Visible);
	ObjectText->SetVisibility(ESlateVisibility::Visible);
	InputKeyImage->SetVisibility(ESlateVisibility::Visible);

	ActionText->SetText(FText::FromString(_ActionName));
	ObjectText->SetText(FText::FromString(_ObjectName));
}

void UDS_InteractWidget::Hide()
{
	ActionText->SetVisibility(ESlateVisibility::Hidden);
	ObjectText->SetVisibility(ESlateVisibility::Hidden);
	InputKeyImage->SetVisibility(ESlateVisibility::Hidden);
}
