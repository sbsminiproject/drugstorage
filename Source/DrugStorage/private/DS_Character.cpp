#include "DS_Character.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"
#include "InputMappingContext.h"
#include "DS_Interactable.h"
#include "DS_PlayerController.h"

ADS_Character::ADS_Character()
{
	PrimaryActorTick.bCanEverTick = true;

	pSpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SPRINGARM"));
	pSpringArmComponent->SetupAttachment(GetCapsuleComponent());
	pSpringArmComponent->TargetArmLength = 0.f;
	pSpringArmComponent->SetRelativeLocation(FVector(0.f, 0.f, 100.f));

	pCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CAMERA"));
	pCameraComponent->SetupAttachment(pSpringArmComponent);

	//Height
	GetCapsuleComponent()->SetCapsuleHalfHeight(130.f);

	//Speed
	GetCharacterMovement()->MaxWalkSpeed = 300.f;

	//Crouch
	GetCharacterMovement()->GetNavAgentPropertiesRef().bCanCrouch = true;
	GetCharacterMovement()->MaxWalkSpeedCrouched = 100.f;

	//Input
	static ConstructorHelpers::FObjectFinder<UInputMappingContext> IMC_Player(TEXT("/Game/Input/IMC_Player.IMC_Player"));
	static ConstructorHelpers::FObjectFinder<UInputAction> IA_Movement(TEXT("/Game/Input/IA_Movement.IA_Movement"));
	static ConstructorHelpers::FObjectFinder<UInputAction> IA_Look(TEXT("/Game/Input/IA_Look.IA_Look"));
	static ConstructorHelpers::FObjectFinder<UInputAction> IA_Crouch(TEXT("/Game/Input/IA_Crouch.IA_Crouch"));
	static ConstructorHelpers::FObjectFinder<UInputAction> IA_Interact(TEXT("/Game/Input/IA_Interact.IA_Interact"));
	if (IMC_Player.Succeeded()) {
		pCharacterContext = IMC_Player.Object;
	}
	if (IA_Movement.Succeeded()) {
		pMoveAction = IA_Movement.Object;
	}
	if (IA_Look.Succeeded()) {
		pLookAction = IA_Look.Object;
	}
	if (IA_Crouch.Succeeded()) {
		pCrouchAction = IA_Crouch.Object;
	}
	if (IA_Interact.Succeeded()) {
		pInteractAction = IA_Interact.Object;
	}
}

void ADS_Character::BeginPlay()
{
	Super::BeginPlay();
	
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())) {
			Subsystem->AddMappingContext(pCharacterContext, 0);
		}
	}

	pController = Cast<ADS_PlayerController>(GetController());
}

void ADS_Character::Tick(float _DeltaTime)
{
	Super::Tick(_DeltaTime);

	FHitResult outHitResult;
	UWorld* currentWorld = GetWorld();
	FCollisionQueryParams collisionParam;
	collisionParam.AddIgnoredActor(this);

	FVector start = pSpringArmComponent->GetComponentLocation() + pCameraComponent->GetForwardVector();
	FVector end = start + pCameraComponent->GetForwardVector() * LineLength;

	currentWorld->LineTraceSingleByChannel(outHitResult, start, end, ECC_Visibility, collisionParam);

	ADS_Interactable* interactable = Cast<ADS_Interactable>(outHitResult.GetActor());
	if (interactable) {
		LookingInteractable(interactable);

		if (pInteractableObject && pInteractableObject == interactable) return;
		StartLookingInteractable(interactable);
	}
	else {
		if (!pInteractableObject) return;
		StopLookingInteractable(interactable);
	}
}

void ADS_Character::SetupPlayerInputComponent(UInputComponent* _PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(_PlayerInputComponent);

	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(_PlayerInputComponent)) {
		EnhancedInputComponent->BindAction(pMoveAction, ETriggerEvent::Triggered, this, &ADS_Character::Move);
		EnhancedInputComponent->BindAction(pLookAction, ETriggerEvent::Triggered, this, &ADS_Character::Look);
		EnhancedInputComponent->BindAction(pCrouchAction, ETriggerEvent::Started, this, &ADS_Character::Crouch);
		EnhancedInputComponent->BindAction(pCrouchAction, ETriggerEvent::Completed, this, &ADS_Character::Crouch);
		EnhancedInputComponent->BindAction(pInteractAction, ETriggerEvent::Started, this, &ADS_Character::Interact);
	}

	pSpringArmComponent->bUsePawnControlRotation = true;
}

void ADS_Character::Move(const FInputActionValue& _Value)
{
	const FVector2D MovementVector = _Value.Get<FVector2D>();

	const FRotator Rotation = Controller->GetControlRotation();
	const FRotator YawRotation(0.f, Rotation.Yaw, 0.f);

	const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
	AddMovementInput(ForwardDirection, MovementVector.Y);

	const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
	AddMovementInput(RightDirection, MovementVector.X);
}

void ADS_Character::Look(const FInputActionValue& _Value)
{
	const FVector2D LookVector = _Value.Get<FVector2D>();

	AddControllerPitchInput(LookVector.Y);
	AddControllerYawInput(LookVector.X);
}

void ADS_Character::Crouch(const FInputActionValue& _Value)
{
	bool isCrouching = _Value.Get<bool>();

	if (isCrouching) {
		Super::Crouch();
	}
	else {
		Super::UnCrouch();
	}
}

//Interact

void ADS_Character::Interact(const FInputActionValue& _Value)
{
	if (!pInteractableObject) return;

	pInteractableObject->Interact(this);
}

void ADS_Character::StartLookingInteractable(ADS_Interactable* _Object)
{
	pInteractableObject = _Object;
	pController->ShowInteractUI(_Object->ActionName, _Object->ObjectName);
}

void ADS_Character::LookingInteractable(ADS_Interactable* _Object)
{
}

void ADS_Character::StopLookingInteractable(ADS_Interactable* _Object)
{
	pInteractableObject = nullptr;
	pController->HideInteractUI();
}

bool ADS_Character::AddItem(FItemState& _ItemState)
{
	if(Inventory.Num() >= 9) return false;
	
	Inventory.Add(_ItemState);
	return true;
}

bool ADS_Character::FindItem(FString _ItemID, FItemState& _Output)
{
	for (const FItemState& Item : Inventory) {
		if (Item.ID.Equals(_ItemID)) {
			_Output = Item;
			return true;
		}
	}

	return false;
}

void ADS_Character::RemoveItem(FItemState& _ItemState)
{
}
