// Fill out your copyright notice in the Description page of Project Settings.


#include "DS_PlayerController.h"
#include "DS_InteractWidget.h"

ADS_PlayerController::ADS_PlayerController()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> WBP_BasicHUD(TEXT("/Game/Widgets/WBP_BasicHUD.WBP_BasicHUD_C"));
	if (WBP_BasicHUD.Succeeded()) {
		BasicHUDClass = WBP_BasicHUD.Class;
	}

	static ConstructorHelpers::FClassFinder<UDS_InteractWidget> WBP_Interact(TEXT("/Game/Widgets/WBP_Interact.WBP_Interact_C"));
	if (WBP_Interact.Succeeded()) {
		InteractUIClass = WBP_Interact.Class;
	}
}

void ADS_PlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (!BasicHUDClass) return;
	pBasicHUD = CreateWidget<UUserWidget>(this, BasicHUDClass);
	pBasicHUD->AddToViewport();

	//interact
	if (!InteractUIClass) return;
	pInteractUI = CreateWidget<UDS_InteractWidget>(this, InteractUIClass);
	pInteractUI->AddToViewport();

	HideInteractUI();
}

void ADS_PlayerController::ShowInteractUI(FString ActionName, FString ObjectName)
{
	if(!pInteractUI) return;

	pInteractUI->Show(ActionName, ObjectName);
}

void ADS_PlayerController::HideInteractUI()
{
	if (!pInteractUI) return;

	pInteractUI->Hide();
}
