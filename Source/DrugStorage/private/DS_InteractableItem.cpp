// Fill out your copyright notice in the Description page of Project Settings.


#include "DS_InteractableItem.h"
#include "DS_Character.h"

void ADS_InteractableItem::Interact(ADS_Character* _Character)
{
	ItemState = { ID, Name, Description };
	bool hasSucceed = _Character->AddItem(ItemState);

	if (hasSucceed) {
		Destroy();
	}
}