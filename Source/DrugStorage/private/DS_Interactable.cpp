// Fill out your copyright notice in the Description page of Project Settings.


#include "DS_Interactable.h"

// Sets default values
ADS_Interactable::ADS_Interactable()
{
	PrimaryActorTick.bCanEverTick = true;

	pMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MESH"));
	SetRootComponent(pMeshComponent);
}


void ADS_Interactable::Interact(ADS_Character* _Character)
{
	DSLOG(Warning, TEXT("Interact"));
}