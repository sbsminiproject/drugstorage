// Copyright Epic Games, Inc. All Rights Reserved.

#include "DrugStorage.h"
#include "Modules/ModuleManager.h"

DEFINE_LOG_CATEGORY(DrugStorage)

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, DrugStorage, "DrugStorage" );
