// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrugStorage.h"
#include "DS_Interactable.h"
#include "DS_InteractableItem.generated.h"

class ADS_Character;

USTRUCT()
struct FItemState {
	GENERATED_BODY()

	FString ID;
	FString Name;
	FString Description;
};

/**
 * 
 */
UCLASS()
class DRUGSTORAGE_API ADS_InteractableItem : public ADS_Interactable
{
	GENERATED_BODY()

protected:
	FItemState ItemState;
	
public:
	UPROPERTY(EditAnywhere)
	FString ID;

	UPROPERTY(EditAnywhere)
	FString Name;

	UPROPERTY(EditAnywhere)
	FString Description;

	virtual void Interact(ADS_Character* _Character) override;
};
