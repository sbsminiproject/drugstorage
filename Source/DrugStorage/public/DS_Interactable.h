// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrugStorage.h"
#include "GameFramework/Actor.h"
#include "DS_Interactable.generated.h"

class ADS_Character;

UCLASS()
class DRUGSTORAGE_API ADS_Interactable : public AActor
{
	GENERATED_BODY()
	
public:	
	ADS_Interactable();

public:
	UPROPERTY(EditAnywhere, Category = Interact)
	FString ActionName;

	UPROPERTY(EditAnywhere, Category = Interact)
	FString ObjectName;

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* pMeshComponent;

public:
	virtual void Interact(ADS_Character* _Character);
};
