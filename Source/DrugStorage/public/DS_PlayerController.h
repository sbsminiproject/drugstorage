// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrugStorage.h"
#include "GameFramework/PlayerController.h"
#include "DS_PlayerController.generated.h"


class UDS_InteractWidget;
/**
 * 
 */
UCLASS()
class DRUGSTORAGE_API ADS_PlayerController : public APlayerController
{
	GENERATED_BODY()
	
	ADS_PlayerController();

public:
	virtual void BeginPlay() override;


private:
	//Basic HUD
	UPROPERTY()
	TSubclassOf<UUserWidget> BasicHUDClass;

	UPROPERTY()
	UUserWidget* pBasicHUD;

	//Interact UI
	UPROPERTY()
	TSubclassOf<UDS_InteractWidget> InteractUIClass;

	UPROPERTY()
	UDS_InteractWidget* pInteractUI;

public:
	void ShowInteractUI(FString ActionName, FString ObjectName);
	void HideInteractUI();
};
