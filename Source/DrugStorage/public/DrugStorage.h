// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "EngineMinimal.h"

DECLARE_LOG_CATEGORY_EXTERN(DrugStorage, Log, All)

#define DSLOG_CALLINFO (FString(__FUNCTION__) + TEXT("(") + FString::FromInt(__LINE__) + TEXT(")"))
#define DSLOG_S(Verbosity) UE_LOG(DrugStorage, Verbosity, TEXT("%s"), *DSLOG_CALLINFO)
#define DSLOG(Verbosity, Format, ...) UE_LOG(DrugStorage, Verbosity, TEXT("%s %s"), *DSLOG_CALLINFO, *FString::Printf(Format, ##__VA_ARGS__))