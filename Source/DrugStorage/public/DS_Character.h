// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrugStorage.h"
#include "GameFramework/Character.h"
#include "InputActionValue.h"
#include "GameFramework\CharacterMovementComponent.h"
#include "DS_InteractableItem.h"
#include "DS_Character.generated.h"

class UInputMappingContext;
class UInputAction;
class ADS_Interactable;
class ADS_PlayerController;

UCLASS()
class DRUGSTORAGE_API ADS_Character : public ACharacter
{
	GENERATED_BODY()

public:
	ADS_Character();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float _DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* _PlayerInputComponent) override;

private:
	UPROPERTY(VisibleAnywhere, Category = Camera, meta = (AllowPrivateAccess = true))
	UCameraComponent* pCameraComponent;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	USpringArmComponent* pSpringArmComponent;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	UInputMappingContext* pCharacterContext;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* pMoveAction;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* pLookAction;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* pCrouchAction;

	UPROPERTY(VisibleAnywhere, Category = Input, meta = (AllowPrivateAccess = true))
	UInputAction* pInteractAction;

private:
	//Interact	
	UPROPERTY()
	ADS_PlayerController* pController;

	UPROPERTY()
	ADS_Interactable* pInteractableObject = nullptr;

	float LineLength = 200.f;

	TArray<FItemState> Inventory;

private:
	void Move(const FInputActionValue& _Value);
	void Look(const FInputActionValue& _Value);
	void Crouch(const FInputActionValue& _Value);

	//Interact
	void Interact(const FInputActionValue& _Value);
	void StartLookingInteractable(ADS_Interactable* _Object);
	void LookingInteractable(ADS_Interactable* _Object);
	void StopLookingInteractable(ADS_Interactable* _Object);

public:
	//Inventory
	bool AddItem(FItemState& _ItemState);
	bool FindItem(FString _ItemID, FItemState& _Output);
	void RemoveItem(FItemState& _ItemState);
};
