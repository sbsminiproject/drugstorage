// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DrugStorageGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DRUGSTORAGE_API ADrugStorageGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
