// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "DS_InteractWidget.generated.h"

/**
 * 
 */
UCLASS()
class DRUGSTORAGE_API UDS_InteractWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* ActionText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UTextBlock* ObjectText;

	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UImage* InputKeyImage;

	void Show(FString _ActionName, FString _ObjectName);
	void Hide();
};
