// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "DrugStorage.h"
#include "DS_Interactable.h"
#include "DS_InteractableDoor.generated.h"


class ADS_Character;
/**
 * 
 */
UCLASS()
class DRUGSTORAGE_API ADS_InteractableDoor : public ADS_Interactable
{
	GENERATED_BODY()

public:
	virtual void Tick(float _DeltaTime) override;

private:
	float DeltaSeconds = 0.f;
	bool IsRotating = false;
	FRotator StartRotation;
	
public:
	UPROPERTY(EditAnywhere, Category = InteractDoor)
	float DoorSpeed = 1.f;
	
	UPROPERTY(EditAnywhere, Category = InteractDoor)
	FRotator EndRotation;

	UPROPERTY(EditAnywhere, Category = InteractDoor)
	FString OpenedText;

	UPROPERTY(EditAnywhere, Category = InteractDoor)
	FString KeyID;

	virtual void Interact(ADS_Character* _Character) override;
};
